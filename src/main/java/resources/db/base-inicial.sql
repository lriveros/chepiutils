CREATE TABLE procesos(
    id      integer auto_increment NOT NULL,
    nombre  varchar(50) NOT NULL,
    url     varchar(150) NOT NULL,
    token   varchar(10) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE servidores(
    id      integer auto_increment NOT NULL,
    nombre  varchar(50) NOT NULL,
    url     varchar(150) NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO procesos VALUES ('PYME KIT - Originales','/soap/services/BCH_PLATDOCPYME_PROCESS/process/processKitPYMEOriginales?wsdl','PYMEKITOR');
INSERT INTO procesos VALUES ('PYME KIT - Copias','/soap/services/BCH_PLATDOCPYME_PROCESS/process/processKitPYMECopias?wsdl','PYMEKITCO');
INSERT INTO procesos VALUES ('PYME Productos - Originales','/soap/services/BCH_PLATDOCPYME_PROCESS/process/processProductosPYMEOriginales?wsdl','PYMECREOR');
INSERT INTO procesos VALUES ('PYME Productos - Copias','/soap/services/BCH_PLATDOCPYME_PROCESS/process/processProductosPYMECopias?wsdl','PYMECRECO');
INSERT INTO procesos VALUES ('PYME Checklist','/soap/services/BCH_CHECKLIST_ADEP/process/processCheckListDocumentos?wsdl','PYMECHK');

INSERT INTO servidores VALUES ('Desarrollo','http://200.14.166.221:8013');
INSERT INTO servidores VALUES ('QA 1','http://200.14.165.235:8011');
INSERT INTO servidores VALUES ('QA 2','http://200.14.165.235:8013');