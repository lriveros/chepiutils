/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.celula.util.servicios;

import cl.bch.adep.personas.cuentavista.copias.BancaPersonaBCHProcessProcessProductoCuentaVistaCopias;
import cl.bch.adep.personas.cuentavista.copias.BancaPersonaBCHProcessProcessProductoCuentaVistaCopiasService;
import cl.bch.adep.personas.kit.banco.BancaPersonaBCHProcessProcessProductoKitBanco;
import cl.bch.adep.personas.kit.banco.BancaPersonaBCHProcessProcessProductoKitBancoService;
import cl.bch.adep.personas.kit.cliente.BancaPersonaBCHProcessProcessProductoKitCliente;
import cl.bch.adep.personas.kit.cliente.BancaPersonaBCHProcessProcessProductoKitClienteService;
import cl.bch.adep.personas.cuentavista.originales.BancaPersonaBCHProcessProcessProductoCuentaVistaOriginales;
import cl.bch.adep.personas.cuentavista.originales.BancaPersonaBCHProcessProcessProductoCuentaVistaOriginalesService;
import cl.celula.servicios.dto.ResultadoPyme;
import java.io.Serializable;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

/**
 *
 * @author ImageMaker
 */
public class ClientePersonasWS implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    public ClientePersonasWS(){
        super();
    }
    
    public ResultadoPyme obtenerProductoKitBanco(String url, String strEntrada) throws Exception{
        BancaPersonaBCHProcessProcessProductoKitBancoService service;
        BancaPersonaBCHProcessProcessProductoKitBanco port;
        ResultadoPyme response = null;

        try {
            service = new BancaPersonaBCHProcessProcessProductoKitBancoService();
            port = service.getProcessProductoKitBanco();
            
            BindingProvider bp = (BindingProvider)port;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
            
            Holder<String> strEncodedDoc = new Holder<>();
            Holder<String> strReason = new Holder<>();
            Holder<String> strResult = new Holder<>();
            
            strEncodedDoc.value = strEntrada;
            
            port.invoke(strEncodedDoc, strReason, strResult);
            response = new ResultadoPyme();
            response.setStrResult(strResult.value);
            response.setStrEncode(strEncodedDoc.value);
            response.setStrReason(strReason.value);
        }
        catch (Exception e) {
            throw e;
        }

        return response;
    }
    
    public ResultadoPyme obtenerProductoKitCliente(String url, String strEntrada) throws Exception{
        BancaPersonaBCHProcessProcessProductoKitClienteService service;
        BancaPersonaBCHProcessProcessProductoKitCliente port;
        ResultadoPyme response = null;

        try {
            service = new BancaPersonaBCHProcessProcessProductoKitClienteService();
            port = service.getProcessProductoKitCliente();
            
            BindingProvider bp = (BindingProvider)port;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
            
            Holder<String> strEncodedDoc = new Holder<>();
            Holder<String> strReason = new Holder<>();
            Holder<String> strResult = new Holder<>();
            
            strEncodedDoc.value = strEntrada;
            
            port.invoke(strEncodedDoc, strReason, strResult);
            response = new ResultadoPyme();
            response.setStrResult(strResult.value);
            response.setStrEncode(strEncodedDoc.value);
            response.setStrReason(strReason.value);
        }
        catch (Exception e) {
            throw e;
        }

        return response;
    }
    
    public ResultadoPyme obtenerProductoCuentaVistaOriginales(String url, String strEntrada) throws Exception{
        BancaPersonaBCHProcessProcessProductoCuentaVistaOriginalesService service;
        BancaPersonaBCHProcessProcessProductoCuentaVistaOriginales port;
        ResultadoPyme response = null;

        try {
            service = new BancaPersonaBCHProcessProcessProductoCuentaVistaOriginalesService();
            port = service.getProcessProductoCuentaVistaOriginales();
            
            BindingProvider bp = (BindingProvider)port;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
            
            Holder<String> strEncodedDoc = new Holder<>();
            Holder<String> strReason = new Holder<>();
            Holder<String> strResult = new Holder<>();
            
            strEncodedDoc.value = strEntrada;
            
            port.invoke(strEncodedDoc, strReason, strResult);
            response = new ResultadoPyme();
            response.setStrResult(strResult.value);
            response.setStrEncode(strEncodedDoc.value);
            response.setStrReason(strReason.value);
        }
        catch (Exception e) {
            throw e;
        }

        return response;
    }
    
    public ResultadoPyme obtenerProductoCuentaVistaCopias(String url, String strEntrada) throws Exception{
        BancaPersonaBCHProcessProcessProductoCuentaVistaCopiasService service;
        BancaPersonaBCHProcessProcessProductoCuentaVistaCopias port;
        ResultadoPyme response = null;

        try {
            service = new BancaPersonaBCHProcessProcessProductoCuentaVistaCopiasService();
            port = service.getProcessProductoCuentaVistaCopias();
            
            BindingProvider bp = (BindingProvider)port;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
            
            Holder<String> strEncodedDoc = new Holder<>();
            Holder<String> strReason = new Holder<>();
            Holder<String> strResult = new Holder<>();
            
            strEncodedDoc.value = strEntrada;
            
            port.invoke(strEncodedDoc, strReason, strResult);
            response = new ResultadoPyme();
            response.setStrResult(strResult.value);
            response.setStrEncode(strEncodedDoc.value);
            response.setStrReason(strReason.value);
        }
        catch (Exception e) {
            throw e;
        }

        return response;
    }
}
