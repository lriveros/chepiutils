package cl.celula.util.servicios;

import cl.bch.adep.pyme.kit.copias.BCHPLATDOCPYMEPROCESSProcessProcessKitPYMECopias;
import cl.bch.adep.pyme.kit.copias.BCHPLATDOCPYMEPROCESSProcessProcessKitPYMECopiasService;
import cl.bch.adep.pyme.kit.originales.BCHPLATDOCPYMEPROCESSProcessProcessKitPYMEOriginales;
import cl.bch.adep.pyme.kit.originales.BCHPLATDOCPYMEPROCESSProcessProcessKitPYMEOriginalesService;
import cl.celula.servicios.dto.ResultadoPyme;
import cl.bch.adep.checklist.BCHCHECKLISTADEPProcessProcessCheckListDocumentos;
import cl.bch.adep.checklist.BCHCHECKLISTADEPProcessProcessCheckListDocumentosService;
import cl.bch.adep.checklist.XML;
import cl.bch.adep.pyme.productos.copias.BCHPLATDOCPYMEPROCESSProcessProcessProductosPYMECopias;
import cl.bch.adep.pyme.productos.copias.BCHPLATDOCPYMEPROCESSProcessProcessProductosPYMECopiasService;
import cl.bch.adep.pyme.productos.originales.BCHPLATDOCPYMEPROCESSProcessProcessProductosPYMEOriginales;
import cl.bch.adep.pyme.productos.originales.BCHPLATDOCPYMEPROCESSProcessProcessProductosPYMEOriginalesService;
import java.io.Serializable;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

/**
 * @author L137
 *
 */
/**
 * @author L137
 */
public class ClientesPymeWS implements Serializable {

    private static final long serialVersionUID = -1336787242923139188L;

    public ClientesPymeWS() {
        super();
    }

    /* PROCESO ADEP ORIGINALES BANCO */
    public ResultadoPyme obtenerCreditoOriginales(String url, String strEntrada) throws Exception {
        BCHPLATDOCPYMEPROCESSProcessProcessProductosPYMEOriginalesService service;
        BCHPLATDOCPYMEPROCESSProcessProcessProductosPYMEOriginales port;
        ResultadoPyme response = null;

        try {
            service = new BCHPLATDOCPYMEPROCESSProcessProcessProductosPYMEOriginalesService();
            port = service.getProcessProductosPYMEOriginales();
            
            BindingProvider bp = (BindingProvider)port;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
            
            Holder<String> strEncodedDoc = new Holder<>();
            Holder<String> strReason = new Holder<>();
            Holder<String> strResult = new Holder<>();
            
            port.invoke(strEntrada, strEncodedDoc, strReason, strResult);
            response = new ResultadoPyme();
            response.setStrResult(strResult.value);
            response.setStrEncode(strEncodedDoc.value);
            response.setStrReason(strReason.value);
        }
        catch (Exception e) {
            throw e;
        }

        return response;
    }

    /* PROCESO ADEP COPIAS CLIENTE Y COPIAS CORREDORA */
    public ResultadoPyme obtenerCreditoCopias(String url, String strEntrada) throws Exception {
        BCHPLATDOCPYMEPROCESSProcessProcessProductosPYMECopiasService service;
        BCHPLATDOCPYMEPROCESSProcessProcessProductosPYMECopias port;
        ResultadoPyme response = null;

        try {
            service = new BCHPLATDOCPYMEPROCESSProcessProcessProductosPYMECopiasService();
            port = service.getProcessProductosPYMECopias();
            
            BindingProvider bp = (BindingProvider)port;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
            
            Holder<String> strEncodedDoc = new Holder<>();
            Holder<String> strReason = new Holder<>();
            Holder<String> strResult = new Holder<>();
            
            port.invoke(strEntrada, strEncodedDoc, strReason, strResult);
            response = new ResultadoPyme();
            response.setStrResult(strResult.value);
            response.setStrEncode(strEncodedDoc.value);
            response.setStrReason(strReason.value);
        }
        catch (Exception e) {
            throw e;
        }

        return response;
    }
    
    /* PROCESS PARA GENERAR CHECK LIST DE DOCUMENTOS SELECCIONADOS EN PANTALLA DE IMPRESION */
    public String obtenerChecklist(String url, String strEntrada) throws Exception {
        BCHCHECKLISTADEPProcessProcessCheckListDocumentosService service;
        BCHCHECKLISTADEPProcessProcessCheckListDocumentos port;
        String response = "";

        try {
            service = new BCHCHECKLISTADEPProcessProcessCheckListDocumentosService();
            port = service.getProcessCheckListDocumentos();
            
            BindingProvider bp = (BindingProvider)port;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
            
            XML theXml = new XML();
            theXml.setDocument(strEntrada);
            response = port.invoke(theXml);
        }
        catch (Exception e) {
            throw e;
        }

        return response;
    }

    // Modificiones Celula_2 Sprint_1

    public ResultadoPyme obtenerKitOriginales(String url, String strEntrada) throws Exception {
        BCHPLATDOCPYMEPROCESSProcessProcessKitPYMEOriginalesService service;
        BCHPLATDOCPYMEPROCESSProcessProcessKitPYMEOriginales port;
        ResultadoPyme response = null;

        try {
            service = new BCHPLATDOCPYMEPROCESSProcessProcessKitPYMEOriginalesService();
            port = service.getProcessKitPYMEOriginales();
            
            BindingProvider bp = (BindingProvider)port;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
            
            Holder<String> strEncodedDoc = new Holder<>();
            Holder<String> strReason = new Holder<>();
            Holder<String> strResult = new Holder<>();
            
            port.invoke(strEntrada, strEncodedDoc, strReason, strResult);
            response = new ResultadoPyme();
            response.setStrResult(strResult.value);
            response.setStrEncode(strEncodedDoc.value);
            response.setStrReason(strReason.value);
        }
        catch (Exception e) {
            throw e;
        }

        return response;
    }

    public ResultadoPyme obtenerKitCopias(String url, String strEntrada) throws Exception {
        BCHPLATDOCPYMEPROCESSProcessProcessKitPYMECopiasService service;
        BCHPLATDOCPYMEPROCESSProcessProcessKitPYMECopias port;
        ResultadoPyme response = null;

        try {
            service = new BCHPLATDOCPYMEPROCESSProcessProcessKitPYMECopiasService();
            port = service.getProcessKitPYMECopias();
            
            Holder<String> strEncodedDoc = new Holder<>();
            Holder<String> strReason = new Holder<>();
            Holder<String> strResult = new Holder<>();
            
            port.invoke(strEntrada, strEncodedDoc, strReason, strResult);
            response = new ResultadoPyme();
            response.setStrResult(strResult.value);
            response.setStrEncode(strEncodedDoc.value);
            response.setStrReason(strReason.value);
        }
        catch (Exception e) {
            throw e;
        }

        return response;
    }



}
