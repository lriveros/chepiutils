package cl.celula.servicios.dto;

import java.io.Serializable;

public class ResultadoPyme implements Serializable{

	private static final long serialVersionUID = -35941144729234999L;
	
	private String strReason = null;
	private String strResult = null;
	private String strEncode = null;
	
	/*GETTERS Y SETTERS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	
	/**
	 * @return the strReason
	 */
	public String getStrReason() {
		return strReason;
	}
	/**
	 * @param strReason the strReason to set
	 */
	public void setStrReason(String strReason) {
		this.strReason = strReason;
	}
	/**
	 * @return the strResult
	 */
	public String getStrResult() {
		return strResult;
	}
	/**
	 * @param strResult the strResult to set
	 */
	public void setStrResult(String strResult) {
		this.strResult = strResult;
	}
	/**
	 * @return the strEncode
	 */
	public String getStrEncode() {
		return strEncode;
	}
	/**
	 * @param strEncode the strEncode to set
	 */
	public void setStrEncode(String strEncode) {
		this.strEncode = strEncode;
	}
	

}
